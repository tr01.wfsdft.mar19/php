<?php
    $num = $_GET['num'];
    $num = strval($num);
    $capicua = false;

    for($i=0; $i<intval((strlen($num)/2)-1); $i++){
        //echo $num[$i];
        for($j=intval((strlen($num)/2)-1); $j>0; $j--){
            if($num[$i] == $num[$j]){
                $capicua = true;
            }else{
                $capicua = false;
            }
        }
    }

    if($capicua){
        echo "El numero $num es capicúa";
    }else{
        echo "El numero $num no es capicúa";
    }